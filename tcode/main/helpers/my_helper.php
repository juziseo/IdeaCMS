<?php

/** * IDEAWEB 企业建站
 * 开发者 连普科技 https://www.lygphp.com/
 */



function dd_mylang() {

}


//转换bbcode to html
function bbc2html($tmpText){
    /*[b]*/ 	$tmpText = preg_replace('#\[b\](.*)\[/b\]#isU', '<strong>$1</strong>', $tmpText);
    /*[i]*/	 	$tmpText = preg_replace('#\[i\](.*)\[/i\]#isU', '<em>$1</em>', $tmpText);
    /*[s]*/	 	$tmpText = preg_replace('#\[s\](.*)\[/s\]#isU', '<del>$1</del>', $tmpText);
    /*[br]*/	$tmpText = preg_replace('#\[br\]#isU', '<br />', $tmpText);
    /*[u]*/	 	$tmpText = preg_replace('#\[u\](.*)\[/u\]#isU', '<span style="text-decoration:underline">$1</span>', $tmpText);
    /*[color]*/ $tmpText = preg_replace('#\[color=(.*)\](.*)\[\/color\]#isU', '<span style="color:$1;">$2</span>', $tmpText);
    /*[size]*/ 	$tmpText = preg_replace('#\[size=([0-9]{1,2})\](.*)\[\/size\]#isU', '<span style="font-size:$1px;">$2</span>', $tmpText);
    /*[font]*/ 	$tmpText = preg_replace('#\[font=(.*)\](.*)\[\/font\]#isU', '<span style="font-family:$1;">$2</span>', $tmpText);
    /*[url=]*/	$tmpText = preg_replace('#\[url=(.*)\](.*)\[\/url\]#isU', '<a href="$1" target="">$2</a>', $tmpText);
    /*[url]*/	$tmpText = preg_replace('#\[url\](.*)\[\/url\]#isU', '<a href="$1" target="">$1</a>', $tmpText);
    /*[img]*/	$tmpText = preg_replace('#\[img\](.*)\[\/img\]#isU', '<img src="$1" alt="Bild" />', $tmpText);
    /*[align]*/ $tmpText = preg_replace('#\[align=(.*)\](.*)\[\/align\]#isU', '<div style="text-align:$1">$2</div>', $tmpText);
    /*[center]*/$tmpText = preg_replace('#\[center\](.*)\[\/center\]#isU', '<div style="text-align:center">$1</div>', $tmpText);
    /*[right]*/ $tmpText = preg_replace('#\[right\](.*)\[\/right\]#isU', '<div style="text-align:right">$1</div>', $tmpText);
    /*[left]*/ 	$tmpText = preg_replace('#\[left\](.*)\[\/left\]#isU', '<div style="text-align:left">$1</div>', $tmpText);
    /*[code]*/ 	$tmpText = preg_replace('#\[code\](.*)\[\/code\]#isU', '<code>$1</code>', $tmpText);
    /*[quote]*/ $tmpText = preg_replace('#\[quote\](.*)\[\/quote\]#isU', '<table width=100% bgcolor=lightgray><tr><td bgcolor=white>$1</td></tr></table>', $tmpText);
    /*[quote=]*/$tmpText = preg_replace('#\[quote=(.*)\](.*)\[\/quote\]#isU', '<table width=100% bgcolor=lightgray><tr><td bgcolor=white>$1<blockquote>$2</blockquote></td></tr></table>', $tmpText);
    /*[mail=]*/	$tmpText = preg_replace('#\[mail=(.*)\](.*)\[\/mail\]#isU', '<a href="mailto:$1">$2</a>', $tmpText);
    /*[mail]*/ 	$tmpText = preg_replace('#\[mail\](.*)\[\/mail\]#isU', '<a href="mailto:$1">$1</a>', $tmpText);
    /*[email=]*/$tmpText = preg_replace('#\[email=(.*)\](.*)\[\/email\]#isU', '<a href="mailto:$1">$2</a>', $tmpText);
    /*[email]*/ $tmpText = preg_replace('#\[email\](.*)\[\/email\]#isU', '<a href="mailto:$1">$1</a>', $tmpText);
    /*[list]*/
    while(preg_match('#\[list\](.*)\[\/list\]#is', $tmpText)){
        $tmpText = preg_replace_callback('#\[list\](.*)\[\/list\]#isU',
            create_function('$str',"return str_replace(array(\"\\r\",\"\\n\"),'','<ul>'.preg_replace('#\[\*\](.*)\$#isU',
				'<li>\$1</li>',preg_replace('#\[\*\](.*)(\<li\>|\$)#isU','<li>\$1</li>\$2',preg_replace('#\[\*\](.*)(\[\*\]|\$)#isU',
				'<li>\$1</li>\$2',\$str[1]))).'</ul>');"), $tmpText);
        $tmpText = preg_replace('#<ul></li>(.*)</ul>(<li>|</ul>)#isU', '<ul>$1</ul></li>$2', $tmpText); // Validitäts-Korrektur
    }
    /*[list=]*/
    while(preg_match('#\[list=(1|a)\](.*?)\[\/list\]#is', $tmpText)){
        $tmpText = preg_replace_callback('#\[list=.\](.*)\[\/list\]#isU',
            create_function('$str',"return str_replace(array(\"\\r\",\"\\n\"),'','<ol>'.preg_replace('#\[\*\](.*)\$#isU',
				'<li>\$1</li>',preg_replace('#\[\*\](.*)(\<li\>|\$)#isU','<li>\$1</li>\$2',preg_replace('/\[\*\](.*?)\[\/\*\]/is',
				'<li>\$1</li>\$2',\$str[1]))).'</ol>');"), $tmpText);
        $tmpText = preg_replace('#<ul></li>(.*)</ul>(<li>|</ul>)#isU', '<ul>$1</ul></li>$2', $tmpText); // Validitäts-Korrektur
    }
    return nl2br($tmpText, true);
}



/*//MIP内容替换 wuuyun mipcontent 
//add by lunzm 20170812 将$data['content']内容生成$data['mipcontent']内容
if(key_exists('content', $data)){
    $data['mipcontent'] =  htmlspecialchars_decode($data['content']);
    preg_match_all('/<img.*?src=[\'|\"](.*?)[\'|\"].*?[\/]?>/', $data['mipcontent'], $imagesArray);
    $patern = '/^http[s]?:\/\/'.
        '(([0-9]{1,3}\.){3}[0-9]{1,3}'.
        '|'.
        '([0-9a-z_!~*\'()-]+\.)*'.
        '([0-9a-z][0-9a-z-]{0,61})?[0-9a-z]\.'.
        '[a-z]{2,6})'.
        '(:[0-9]{1,4})?'.
        '((\/\?)|'.
        '(\/[0-9a-zA-Z_!~\*\'\(\)\.;\?:@&=\+\$,%#-\/]*)?)$/';
    foreach($imagesArray[0] as $key => $val) {
        @preg_match('/alt=".+?"/',$val,$tempAlt);
        @$alt = explode('=',$tempAlt[0]);
        @$alt = explode('"',$alt[1]);
        if (count($alt) == 1) {
            $alt = $alt[0];
        }
        if (count($alt) == 2) {
            $alt = $alt[1] ;
        }
        if (count($alt) == 3) {
            $alt = $alt[1] ;
        }
        if (@preg_match($patern,$imagesArray[1][$key])) {
            $src = $imagesArray[1][$key];
        } else {
            $src = 'http://'.$_SERVER['SERVER_NAME'].'/'.$imagesArray[1][$key];
        }
        $layout = 'layout="container"';
        $tempImg = '<mip-img '.$layout.' alt="'.$alt.'" src="'.$src.'" popup></mip-img>';
        $data['mipcontent'] =  str_replace($val,$tempImg,$data['mipcontent']);
    }
    $data['mipcontent'] =  preg_replace("/style=.+?['|\"]/i",'', bbc2html($data['mipcontent']));
    @preg_match_all('/<a[^>]*>[^>]+a>/',$data['mipcontent'],$tempLink);
    foreach($tempLink[0] as $k => $v) {
        if(strpos($v,"href")) {
            @preg_match('/href\s*=\s*(?:"([^"]*)"|\'([^\']*)\'|([^"\'>\s]+))/',$v,$hrefRes);
            $matches = @preg_match($patern,$hrefRes[1]);
            if (!$matches) {
                $data['mipcontent'] = str_replace($v,'',$data['mipcontent']);
            }
        } else {
            $data['mipcontent'] = str_replace($v,'',$data['mipcontent']);
        }
    }
    @preg_match_all('/<iframe.*?src=[\'|\"](.*?)[\'|\"].*?[\/]?>*<\/iframe>/', $data['mipcontent'], $iframeArray);
    if ($iframeArray) {
        foreach($iframeArray[0] as $key => $val) {
            $layout = 'layout="responsive"';
            $tempiframe = '<mip-iframe  width="320" height="200" '.$layout.' src="'.$iframeArray[1][$key].'"></mip-iframe>';
            $data['mipcontent'] =  str_replace($val,$tempiframe,$data['mipcontent']);
        }
    }
    @preg_match_all('/<embed.*?src=[\'|\"](.*?)[\'|\"].*?[\/]?>/',$data['mipcontent'], $embedArray);
    if ($embedArray) {
        foreach($embedArray[0] as $key => $val) {
            $layout = '';
            $tempembed = '<mip-embed type="ad-comm" '.$layout.' src="'.$embedArray[1][$key].'"></mip-embed>';
            $data['mipcontent'] =  str_replace($val,$tempembed,$data['mipcontent']);
        }
    }
    @preg_match_all('/<video.*?src=[\'|\"](.*?)[\'|\"].*?[\/]?>*<\/video>/', $data['mipcontent'], $videoArray);
    if ($videoArray) {
        foreach($videoArray[0] as $key => $val) {
            $layout = '';
            $tempvideo = '<mip-video poster="https://placehold.it/640x360" controls
layout="responsive" width="640" height="360" '.$layout.' src="'.$videoArray[1][$key].'"></mip-video>';
            $data['mipcontent'] =  str_replace($val,$tempvideo,$data['mipcontent']);
        }
    }
    @preg_match_all('/<a.*?[\/]?>/', $data['mipcontent'], $aArray);
    if ($aArray) {
        foreach($aArray[0] as $key => $val) {
            if(stripos($val, 'title') !== false){
                @preg_match_all('/title="(.+?)"/', $val, $titleArray);
                $aReplace = preg_replace('/title=".+?"/', 'data-title="'.$titleArray[1][0].'"',$val);
                $data['mipcontent'] =  str_replace($val,$aReplace,$data['mipcontent']);
            }
        }
    }
}
//end wuuyun mipcontent*/