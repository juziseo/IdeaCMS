<?php

/** * IDEAWEB 企业建站
 * 开发者 连普科技 https://www.lygphp.com/
 */
 
class Home extends M_Controller {

    /**
     * 首页
     */
    public function index() {

        $this->template->assign(array(
            'indexc' => 1,
            'meta_title' => SITE_TITLE,
            'meta_keywords' => SITE_KEYWORDS,
            'meta_description' => SITE_DESCRIPTION,
        ));
        $this->template->display('index.html');
    }

}