<?php



class F_Textarea extends A_Field {
	
	/**
     * 构造函数
     */
    public function __construct() {
		parent::__construct();
		$this->name = fc_lang('多行文本'); // 字段名称
		$this->fieldtype = array(
			'TEXT' => ''
		); // TRUE表全部可用字段类型,自定义格式为 array('可用字段类型名称' => '默认长度', ... )
		$this->defaulttype = 'TEXT'; // 当用户没有选择字段类型时的缺省值
    }
	
	/**
	 * 字段相关属性参数
	 *
	 * @param	array	$value	值
	 * @return  string
	 */
	public function option($option) {
		$option['width'] = isset($option['width']) ? $option['width'] : 300;
		$option['height'] = isset($option['height']) ? $option['height'] : 100;
		$option['value'] = isset($option['value']) ? $option['value'] : '';
		$option['fieldtype'] = isset($option['fieldtype']) ? $option['fieldtype'] : '';
		$option['is_mb_auto'] = isset($option['is_mb_auto']) ? $option['is_mb_auto'] : '';
		$option['fieldlength'] = isset($option['fieldlength']) ? $option['fieldlength'] : '';
		return '
			<div class="layui-form-item">
				<label class="layui-form-label">'.fc_lang('宽度').'：</label>
				<div class="layui-col-md2">
					<input type="text" class="layui-input" name="data[setting][option][width]" value="'.$option['width'].'">
				</div>
				<div class="layui-form-mid layui-word-aux layui-col-md3">'.fc_lang('[整数]表示固定宽带；[整数%]表示百分比').'</div>
			</div>
            <div class="layui-form-item">
                <label class="layui-form-label">'.fc_lang('移动端宽度').'：</label>
                <div class="layui-col-md4">
					<input type="radio" value="0" title="'.fc_lang('自动').'" name="data[setting][option][is_mb_auto]" '.(!$option['is_mb_auto'] ? 'checked' : '').'> 
					<input type="radio" value="1" title="'.fc_lang('不自动').'" name="data[setting][option][is_mb_auto]" '.($option['is_mb_auto'] ? 'checked' : '').'> 
                </div>
                <div class="layui-form-mid layui-word-aux">自动 100%宽度</div>
            </div>
			<div class="layui-form-item">
				<label class="layui-form-label">'.fc_lang('高度').'：</label>
				<div class="layui-col-md2">
					<input type="text" class="layui-input" name="data[setting][option][height]" value="'.$option['height'].'"> 
				</div>
				<div class="layui-form-mid layui-word-aux">px</div>
			</div>
			<div class="layui-form-item">
				<label class="layui-form-label">'.fc_lang('默认值').'：</label>
				<div class="layui-col-md3">
					<input id="field_default_value" type="text" class="layui-input" size="20" value="'.$option['value'].'" name="data[setting][option][value]">
				</div>
				<div class="layui-form-mid layui-word-aux"><div class="layui-col-md4">'.$this->member_field_select().'</div>'.fc_lang('用当前登录会员信息来填充这个值').'</div>
			</div>
			'.$this->field_type($option['fieldtype'], $option['fieldlength']);
	}
	
	/**
	 * 字段表单输入
	 *
	 * @param	string	$cname	字段别名
	 * @param	string	$name	字段名称
	 * @param	array	$cfg	字段配置
	 * @param	array	$value	值
	 * @return  string
	 */
	public function input($cname, $name, $cfg, $value = NULL, $id = 0) {
		// 字段显示名称
		$text = (isset($cfg['validate']['required']) && $cfg['validate']['required'] == 1 ? '<font color="red">*</font>' : '').''.$cname.'：';
		// 表单宽度设置
		if (IS_MOBILE && empty($cfg['option']['is_mb_auto'])) {
			$width = '100%';
		} else {
			$width = isset($cfg['option']['width']) && $cfg['option']['width'] ? $cfg['option']['width'] : '300';
		}
		// 表单高度设置
		$height = isset($cfg['option']['height']) && $cfg['option']['height'] ? $cfg['option']['height'] : '100';
		// 表单附加参数
		$attr = isset($cfg['validate']['formattr']) && $cfg['validate']['formattr'] ? $cfg['validate']['formattr'] : '';
		// 字段提示信息
		$tips = isset($cfg['validate']['tips']) && $cfg['validate']['tips'] ? '<div class="layui-form-mid layui-word-aux layui-col-md3" id="dd_'.$name.'_tips">'.$cfg['validate']['tips'].'</div>' : '';
		// 字段默认值
		$value = strlen($value) ? $value : $this->get_default_value($cfg['option']['value']);// 禁止修改
		$disabled = !IS_ADMIN && $id && $value && isset($cfg['validate']['isedit']) && $cfg['validate']['isedit'] ? 'disabled' : ''; 
		$str = '<textarea class="layui-input" '.$disabled.' style="height:'.$height.'px; width:'.$width.(is_numeric($width) ? 'px' : '').';" name="data['.$name.']" id="dd_'.$name.'" '.$attr.'>'.$value.'</textarea>'.$tips;
		return $this->input_format($name, $text, $str);
	}
	
}