<?php

/** * IDEAWEB 企业建站
 * 开发者 连普科技 https://www.lygphp.com/
 */

return array(

    array(
        'name' => '系统',
        'mark' => 'home',
        'icon' => 'fa fa-cogs',
        'menu' => array(
            array(
                'name' => '控制台',
                'mark' => 'home-home',
                'icon' => 'fa fa-home',
                'menu' => array(
                    array(
                        'name' => '后台首页',
                        'uri' => 'home/main',
                        'icon' => 'fa fa-home',
                    ),
                    array(
                        'name' => '资料修改',
                        'uri' => 'root/my',
                        'icon' => 'fa fa-user',
                    ),
                    array(
                        'name' => '错误日志',
                        'uri' => 'system/debug',
                        'icon' => 'fa fa-bug',
                    ),
                    array(
                        'name' => '操作日志',
                        'uri' => 'system/index',
                        'icon' => 'fa fa-calendar',
                    ),
                )
            ),
            array(
                'name' => '系统设置',
                'mark' => 'cog-sys',
                'icon' => 'fa fa-cog',
                'menu' => array(
                    array(
                        'name' => '后台设置',
                        'uri' => 'system/config',
                        'icon' => 'fa fa-cog',
                    ),
                    array(
                        'name' => '邮件设置',
                        'uri' => 'mail/index',
                        'icon' => 'fa fa-envelope',
                    ),
                    array(
                        'name' => '管理员管理',
                        'uri' => 'root/index',
                        'icon' => 'fa fa-user',
                    ),
                )
            ),
        )
    ),


    array(
        'name' => '网站',
        'mark' => 'content',
        'icon' => 'fa fa-internet-explorer',
        'menu' => array(
            array(
                'name' => '内容管理',
                'mark' => 'content-content',
                'icon' => 'fa fa-th-large',
                'menu' => array(
                    array(
                        'name' => '栏目管理',
                        'uri' => 'category/index',
                        'icon' => 'fa fa-list',
                    ),
                    array(
                        'name' => '关键词库',
                        'uri' => 'tag/index',
                        'icon' => 'fa fa-tag',
                    ),
                    array(
                        'name' => '附件管理',
                        'uri' => 'attachment/index',
                        'icon' => 'fa fa-folder',
                    ),
                    array(
                        'name' => '自定义内容',
                        'uri' => 'block/index',
                        'icon' => 'fa fa-th-large',
                    ),
                    array(
                        'name' => 'URL伪静态',
                        'uri' => 'urlrule/index',
                        'icon' => 'fa fa-magnet',
                    ),
                    array(
                        'name' => '模型管理',
                        'uri' => 'module/index',
                        'icon' => 'fa fa-cubes',
                    ),
                )
            ),
            array(
                'name' => '表单管理',
                'mark' => 'content-form',
                'icon' => 'fa fa-th-large',
                'menu' => array(
                    array(
                        'name' => '网站表单',
                        'uri' => 'form/index',
                        'icon' => 'fa fa-tasks',
                    ),
 
                )
            ),
            array(
                'name' => '网站设置',
                'mark' => 'cog-sys',
                'icon' => 'fa fa-cog',
                'menu' => array(
                    array(
                        'name' => '网站设置',
                        'uri' => 'site/config',
                        'icon' => 'fa fa-cog',
                    ),
                    array(
                        'name' => '网站管理',
                        'uri' => 'site/index',
                        'icon' => 'fa fa-globe',
                    ),
                )
            ),

        )
    ),

    array(
        'name' => '微信',
        'mark' => 'weixin',
        'icon' => 'fa fa-weixin',
        'menu' => array(
            array(
                'name' => '微信管理',
                'mark' => 'weixin-weixin',
                'icon' => 'fa fa-weixin',
                'menu' => array(

                    array(
                        'name' => '账号接入',
                        'uri' => 'weixin/index',
                        'icon' => 'fa fa-cog',
                    ),
                    array(
                        'name' => '自定义菜单',
                        'uri' => 'wmenu/index',
                        'icon' => 'fa fa-table',
                    ),
                    array(
                        'name' => '微信粉丝',
                        'uri' => 'wuser/index',
                        'icon' => 'fa fa-user',
                    ),
                )
            ),

        )
    ),

    array(
        'name' => '扩展',
        'mark' => 'myapp',
        'icon' => 'fa fa-puzzle-piece',
        'menu' => array(
            array(
                'name' => '扩展管理',
                'mark' => 'app',
                'icon' => 'fa fa-puzzle-piece',
                'menu' => array(
                    array(
                        'name' => '联动菜单',
                        'uri' => 'linkage/index',
                        'icon' => 'fa fa-windows',
                    ),
                    array(
                        'name' => '数据结构',
                        'uri' => 'db/index',
                        'icon' => 'fa fa-database',
                    ),
                )
            ),

        )
    ),

);