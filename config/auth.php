<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 *  后台权限控制
 *  配置方法 将admin目录下的每个控制器的每个方法都按照下面格式写进来
 */

$config['auth'][] = array(
	'auth' => array(
		'admin/attachment/index' => fc_lang('管理附件'),
        'admin/attachment/result' => fc_lang('搜索附件'),
		'admin/attachment/del' => fc_lang('删除附件'),
	)
);

$config['auth'][] = array(
	'auth' => array(
		'admin/block/index' => fc_lang('管理自定义内容'),
        'admin/block/add' => fc_lang('添加自定义内容'),
		'admin/block/edit' => fc_lang('编辑自定义内容'),
		'admin/block/del' => fc_lang('删除自定义内容'),
	)
);

$config['auth'][] = array(
	'auth' => array(
		'admin/category/index' => fc_lang('管理栏目'),
        'admin/category/add' => fc_lang('添加添加栏目'),
		'admin/category/edit' => fc_lang('编辑栏目'),
		'admin/category/delete' => fc_lang('删除栏目'),
		'admin/category/select' => fc_lang('下拉选择栏目'),
		'admin/category/cache' => fc_lang('更新栏目缓存'),
		'admin/category/select_category' => fc_lang('选择栏目'),
		'admin/category/url' => fc_lang('自定义URL'),
	)
);

$config['auth'][] = array(
	'auth' => array(
		'admin/content/index' => fc_lang('管理内容'),
        'admin/content/add' => fc_lang('添加内容'),
		'admin/content/edit' => fc_lang('编辑内容'),
		'admin/content/status' => fc_lang('设定内容状态'),
		'admin/content/url' => fc_lang('更新内容URL'),
		'admin/content/del' => fc_lang('删除内容'),
		'admin/content/_get_field' => fc_lang('获取字段'),
	)
);

$config['auth'][] = array(
	'auth' => array(
		'admin/db/index' => fc_lang('管理数据表'),
        'admin/db/tableshow' => fc_lang('查看表结构'),
	)
);

$config['auth'][] = array(
	'auth' => array(
		'admin/fcontent/index' => fc_lang('管理表单内容'),
        'admin/fcontent/add' => fc_lang('添加表单内容'),
		'admin/fcontent/edit' => fc_lang('编辑表单内容'),
		'admin/fcontent/del' => fc_lang('删除表单内容'),
	)
);

$config['auth'][] = array(
	'auth' => array(
		'admin/field/index' => fc_lang('管理字段'),
        'admin/field/add' => fc_lang('添加字段'),
		'admin/field/edit' => fc_lang('编辑字段'),
		'admin/field/del' => fc_lang('删除字段'),
		'admin/field/option' => fc_lang('通用操作'),
	)
);

$config['auth'][] = array(
	'auth' => array(
		'admin/form/index' => fc_lang('管理表单'),
        'admin/form/add' => fc_lang('添加表单'),
		'admin/form/edit' => fc_lang('编辑表单'),
		'admin/form/del' => fc_lang('删除表单'),
		'admin/form/cache' => fc_lang('更新缓存'),
	)
);

$config['auth'][] = array(
	'auth' => array(
		'admin/form/index' => fc_lang('管理表单'),
        'admin/form/add' => fc_lang('添加表单'),
		'admin/form/edit' => fc_lang('编辑表单'),
		'admin/form/del' => fc_lang('删除表单'),
		'admin/form/cache' => fc_lang('更新缓存'),
	)
);

$config['auth'][] = array(
	'auth' => array(
		'admin/mail/index' => fc_lang('管理邮件'),
        'admin/mail/add' => fc_lang('添加邮件'),
		'admin/mail/edit' => fc_lang('编辑邮件'),
		'admin/mail/del' => fc_lang('删除邮件'),
		'admin/mail/test' => fc_lang('删除邮件'),
		'admin/mail/cache' => fc_lang('更新缓存'),
	)
);

$config['auth'][] = array(
	'auth' => array(
		'admin/root/index' => fc_lang('管理管理员'),
        'admin/root/add' => fc_lang('添加管理员'),
		'admin/root/edit' => fc_lang('编辑管理员'),
		'admin/root/del' => fc_lang('删除管理员'),
		'admin/root/check_username' => fc_lang('检查用户情况'),
	)
);

$config['auth'][] = array(
	'auth' => array(
		'admin/route/index' => fc_lang('管理路由'),
        'admin/route/todo' => fc_lang('生成路由'),
	)
);

$config['auth'][] = array(
	'auth' => array(
		'admin/site/index' => fc_lang('管理站点'),
        'admin/site/add' => fc_lang('添加站点'),
		'admin/site/edit' => fc_lang('编辑站点'),
		'admin/site/del' => fc_lang('删除站点'),
		'admin/site/config' => fc_lang('配置站点'),
		'admin/site/check_username' => fc_lang('检查用户情况'),
	)
);

$config['auth'][] = array(
	'auth' => array(
		'admin/system/index' => fc_lang('系统日志'),
        'admin/system/debug' => fc_lang('系统错误日志'),
		'admin/system/syskey' => fc_lang('安全码'),
		'admin/system/referer' => fc_lang('来路随机串'),
		'admin/system/config' => fc_lang('配置系统'),
	)
);

$config['auth'][] = array(
	'auth' => array(
		'admin/site/index' => fc_lang('管理Tag'),
        'admin/site/add' => fc_lang('添加Tag'),
		'admin/site/all_add' => fc_lang('批量添加Tag'),
		'admin/site/del' => fc_lang('删除站点'),
	)
);

$config['auth'][] = array(
	'auth' => array(
		'admin/urlrule/index' => fc_lang('管理伪静态'),
        'admin/urlrule/copy' => fc_lang('复制伪静态'),
		'admin/urlrule/add' => fc_lang('添加伪静态'),
		'admin/urlrule/edit' => fc_lang('编辑伪静态'),
		'admin/urlrule/cache' => fc_lang('更新伪静态缓存'),
	)
);

$config['auth'][] = array(
	'auth' => array(
		'admin/weixin/index' => fc_lang('配置公众号'),
        'admin/weixin/reply' => fc_lang('关键词回复'),
		'admin/weixin/ajaxload' => fc_lang('上传文件'),
		'admin/weixin/ts' => fc_lang('推送'),
		'admin/weixin/cache' => fc_lang('更新缓存'),
	)
);

$config['auth'][] = array(
	'auth' => array(
		'admin/weixin/index' => fc_lang('配置公众号'),
        'admin/weixin/reply' => fc_lang('关键词回复'),
		'admin/weixin/ajaxload' => fc_lang('上传文件'),
		'admin/weixin/ts' => fc_lang('推送'),
		'admin/weixin/cache' => fc_lang('更新缓存'),
	)
);